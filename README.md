# Mini-Project3
An IaC project that uses [AWS CDK](https://aws.amazon.com/cdk/) (Cloud Development Kit) with CodeWhisperer to create an AWS S3 bucket. [CodeCatalyst](https://codecatalyst.aws/explore) is used as the integrated development environment, and Typescript is used for the CDK. 

## Usage
`cdk synth`: Synthesize an AWS CloudFormation template for the app

`cdk deploy`: Deploy the stack using AWS CloudFormation


## CodeWhisperer
Amazon CodeWhisperer is an AI-powered code generator which can give suggestions ranging from snippets to full functions in real time in the IDE based on the provided comments and existing code. 

In this project, CodeWhisperer is used to generate the CDK code that creates the S3 bucket, which is in `lib/cdk-app-stack.ts`. 

```
export class CdkAppStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // create an Amazon S3 bucket using the Bucket construct
    new  s3.Bucket(this, 'CDKAppBucket', {
      // add version
      versioned: true,
      // add encryption
      encryption: s3.BucketEncryption.KMS_MANAGED,
    });
  }
}
```
By providing appropriate comments, CodeWhisperer is able to generate the corresponding code. 

## AWS S3 Bucket
The S3 bucket has versioning and encryption enabled, as described by the CDK code. After deploying the app, a S3 bucket is automatically created. 

![S3_Bucket](assets/s3_bucket.jpg)


