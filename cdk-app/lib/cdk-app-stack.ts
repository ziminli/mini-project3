import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { aws_s3 as s3 } from 'aws-cdk-lib';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class CdkAppStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // create an Amazon S3 bucket using the Bucket construct
    new  s3.Bucket(this, 'CDKAppBucket', {
      // add version
      versioned: true,
      // add encryption
      encryption: s3.BucketEncryption.KMS_MANAGED,
    });
  }
}
